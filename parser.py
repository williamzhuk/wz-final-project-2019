
in_path = "pems.dot.ca.gov.txt"
out_path = "BayStations.txt"
pems_file = open(in_path, "r")

out_lines = []

columns = pems_file.readline().split("\t")
sensors = set()
print(columns)
for line in pems_file.readlines():
    tokens = line.split("\t")

    sensor_type = tokens[columns.index("Sensor Type")]
    sensors.add(sensor_type)

    out_lines += tokens[columns.index('ID')] + "\n"

print(sensors)
out_file = open(out_path, "w")
out_file.writelines(out_lines)