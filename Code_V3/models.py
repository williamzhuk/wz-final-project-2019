from collections import OrderedDict

from torch_geometric.nn import GATConv, MessagePassing, GCNConv, GINConv, SAGEConv
from torch_geometric.nn.inits import glorot, zeros
from torch_geometric.utils import add_remaining_self_loops
from torch_scatter import scatter_add
from torch.nn import LSTMCell
import torch.utils.data as utils
import torch.nn.functional as F
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn.parameter import Parameter
import numpy as np
import pandas as pd
import math
import time
from scipy import stats
from Code_V1.Models import FilterLinear

## Existing Models that were in codebase before

class LSTM(nn.Module):
    def __init__(self, input_size, cell_size, hidden_size, output_last=True):
        """
        cell_size is the size of cell_state.
        hidden_size is the size of hidden_state, or say the output_state of each step
        """
        super(LSTM, self).__init__()

        self.cell_size = cell_size
        self.hidden_size = hidden_size
        self.fl = nn.Linear(input_size + hidden_size, hidden_size)
        self.il = nn.Linear(input_size + hidden_size, hidden_size)
        self.ol = nn.Linear(input_size + hidden_size, hidden_size)
        self.Cl = nn.Linear(input_size + hidden_size, hidden_size)

        self.output_last = output_last

    def step(self, input, Hidden_State, Cell_State):
        combined = torch.cat((input, Hidden_State), 1)
        f = torch.sigmoid(self.fl(combined))
        i = torch.sigmoid(self.il(combined))
        o = torch.sigmoid(self.ol(combined))
        C = torch.tanh(self.Cl(combined))
        Cell_State = f * Cell_State + i * C
        Hidden_State = o * torch.tanh(Cell_State)

        return Hidden_State, Cell_State

    def forward(self, inputs):
        batch_size = inputs.size(0)
        time_step = inputs.size(1)
        Hidden_State, Cell_State = self.initHidden(batch_size)

        if self.output_last:
            for i in range(time_step):
                Hidden_State, Cell_State = self.step(torch.squeeze(inputs[:, i:i + 1, :]), Hidden_State, Cell_State)
            return Hidden_State
        else:
            outputs = None
            for i in range(time_step):
                Hidden_State, Cell_State = self.step(torch.squeeze(inputs[:, i:i + 1, :]), Hidden_State, Cell_State)
                if outputs is None:
                    outputs = Hidden_State.unsqueeze(1)
                else:
                    outputs = torch.cat((outputs, Hidden_State.unsqueeze(1)), 1)
            return outputs

    def initHidden(self, batch_size):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size))
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size))
            return Hidden_State, Cell_State


class ConvLSTM(nn.Module):
    def __init__(self, input_size, cell_size, hidden_size, output_last=True):
        """
        cell_size is the size of cell_state.
        hidden_size is the size of hidden_state, or say the output_state of each step
        """
        super(ConvLSTM, self).__init__()

        self.cell_size = cell_size
        self.hidden_size = hidden_size
        self.fl = nn.Linear(input_size + hidden_size, hidden_size)
        self.il = nn.Linear(input_size + hidden_size, hidden_size)
        self.ol = nn.Linear(input_size + hidden_size, hidden_size)
        self.Cl = nn.Linear(input_size + hidden_size, hidden_size)

        self.conv = nn.Conv1d(1, hidden_size, hidden_size)

        self.output_last = output_last

    def step(self, input, Hidden_State, Cell_State):

        conv = self.conv(input)

        combined = torch.cat((conv, Hidden_State), 1)
        f = torch.sigmoid(self.fl(combined))
        i = torch.sigmoid(self.il(combined))
        o = torch.sigmoid(self.ol(combined))
        C = torch.tanh(self.Cl(combined))
        Cell_State = f * Cell_State + i * C
        Hidden_State = o * torch.tanh(Cell_State)

        return Hidden_State, Cell_State

    def forward(self, inputs):
        batch_size = inputs.size(0)
        time_step = inputs.size(1)
        Hidden_State, Cell_State = self.initHidden(batch_size)

        if self.output_last:
            for i in range(time_step):
                Hidden_State, Cell_State = self.step(torch.squeeze(inputs[:, i:i + 1, :]), Hidden_State, Cell_State)
            return Hidden_State
        else:
            outputs = None
            for i in range(time_step):
                Hidden_State, Cell_State = self.step(torch.squeeze(inputs[:, i:i + 1, :]), Hidden_State, Cell_State)
                if outputs is None:
                    outputs = Hidden_State.unsqueeze(1)
                else:
                    outputs = torch.cat((outputs, Hidden_State.unsqueeze(1)), 1)
            return outputs

    def initHidden(self, batch_size):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size))
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size))
            return Hidden_State, Cell_State


class LocalizedSpectralGraphConvolution(nn.Module):
    def __init__(self, A, K):

        super(LocalizedSpectralGraphConvolution, self).__init__()

        self.K = K
        self.A = A.cuda()
        feature_size = A.shape[0]
        self.D = torch.diag(torch.sum(self.A, dim=0)).cuda()

        I = torch.eye(feature_size, feature_size).cuda()
        self.L = I - torch.inverse(torch.sqrt(self.D)).matmul(self.A).matmul(torch.inverse(torch.sqrt(self.D)))

        L_temp = I
        for i in range(K):
            L_temp = torch.matmul(L_temp, self.L)
            if i == 0:
                self.L_tensor = torch.unsqueeze(L_temp, 2)
            else:
                self.L_tensor = torch.cat((self.L_tensor, torch.unsqueeze(L_temp, 2)), 2)

        self.L_tensor = Variable(self.L_tensor.cuda(), requires_grad=False)

        self.params = Parameter(torch.FloatTensor(K).cuda())

        stdv = 1. / math.sqrt(K)
        for i in range(K):
            self.params[i].data.uniform_(-stdv, stdv)

    def forward(self, input):
        x = input

        conv = x.matmul(torch.sum(self.params.expand_as(self.L_tensor) * self.L_tensor, 2))

        return conv


class LocalizedSpectralGraphConvolutionalLSTM(nn.Module):

    def __init__(self, K, A, feature_size, Clamp_A=True, output_last=True):
        '''
        Args:
            K: K-hop graph
            A: adjacency matrix
            FFR: free-flow reachability matrix
            feature_size: the dimension of features
            Clamp_A: Boolean value, clamping all elements of A between 0. to 1.
        '''
        super(LocalizedSpectralGraphConvolutionalLSTM, self).__init__()
        self.feature_size = feature_size
        self.hidden_size = feature_size

        self.K = K
        self.A = A
        self.gconv = LocalizedSpectralGraphConvolution(A, K)

        hidden_size = self.feature_size
        input_size = self.feature_size + hidden_size

        self.fl = nn.Linear(input_size, hidden_size)
        self.il = nn.Linear(input_size, hidden_size)
        self.ol = nn.Linear(input_size, hidden_size)
        self.Cl = nn.Linear(input_size, hidden_size)

        self.output_last = output_last

    def step(self, input, Hidden_State, Cell_State):

        #         conv_sample_start = time.time()
        conv = F.relu(self.gconv(input))
        #         conv_sample_end = time.time()
        #         print('conv_sample:', (conv_sample_end - conv_sample_start))
        combined = torch.cat((conv, Hidden_State), 1)
        f = torch.sigmoid(self.fl(combined))
        i = torch.sigmoid(self.il(combined))
        o = torch.sigmoid(self.ol(combined))
        C = torch.tanh(self.Cl(combined))
        Cell_State = f * Cell_State + i * C
        Hidden_State = o * torch.tanh(Cell_State)

        return Hidden_State, Cell_State

    def Bi_torch(self, a):
        a[a < 0] = 0
        a[a > 0] = 1
        return a

    def forward(self, inputs):
        batch_size = inputs.size(0)
        time_step = inputs.size(1)
        Hidden_State, Cell_State = self.initHidden(batch_size)

        outputs = None

        for i in range(time_step):
            Hidden_State, Cell_State = self.step(torch.squeeze(inputs[:, i:i + 1, :]), Hidden_State, Cell_State)

            if outputs is None:
                outputs = Hidden_State.unsqueeze(1)
            else:
                outputs = torch.cat((outputs, Hidden_State.unsqueeze(1)), 1)
        #         print(type(outputs))

        if self.output_last:
            return outputs[:, -1, :]
        else:
            return outputs

    def initHidden(self, batch_size):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size))
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size))
            return Hidden_State, Cell_State

    def reinitHidden(self, batch_size, Hidden_State_data, Cell_State_data):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(Hidden_State_data.cuda(), requires_grad=True)
            Cell_State = Variable(Cell_State_data.cuda(), requires_grad=True)
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(Hidden_State_data, requires_grad=True)
            Cell_State = Variable(Cell_State_data, requires_grad=True)
            return Hidden_State, Cell_State


class SpectralGraphConvolution(nn.Module):
    def __init__(self, A):
        super(SpectralGraphConvolution, self).__init__()

        feature_size = A.shape[0]

        self.A = A
        self.D = torch.diag(torch.sum(self.A, dim=0))
        self.L = self.D - A
        self.param = Parameter(torch.FloatTensor(feature_size).cuda())
        stdv = 1. / math.sqrt(feature_size)
        self.param.data.uniform_(-stdv, stdv)

        self.e, self.v = torch.eig(self.L, eigenvectors=True)
        self.vt = torch.t(self.v)
        self.v = Variable(self.v.cuda(), requires_grad=False)
        self.vt = Variable(self.vt.cuda(), requires_grad=False)

    def forward(self, input):
        x = input
        conv_sample_start = time.time()
        conv = x.matmul(self.v.matmul(torch.diag(self.param)).matmul(self.vt))
        conv_sample_end = time.time()
        # print('conv_sample:', (conv_sample_end - conv_sample_start))
        return conv


class SpectralGraphConvolutionalLSTM(nn.Module):

    def __init__(self, K, A, feature_size, Clamp_A=True, output_last=True):
        '''
        Args:
            K: K-hop graph
            A: adjacency matrix
            FFR: free-flow reachability matrix
            feature_size: the dimension of features
            Clamp_A: Boolean value, clamping all elements of A between 0. to 1.
        '''
        super(SpectralGraphConvolutionalLSTM, self).__init__()
        self.feature_size = feature_size
        self.hidden_size = feature_size

        self.K = K
        self.A = A
        self.gconv = SpectralGraphConvolution(A)

        hidden_size = self.feature_size
        input_size = self.feature_size + hidden_size

        self.fl = nn.Linear(input_size, hidden_size)
        self.il = nn.Linear(input_size, hidden_size)
        self.ol = nn.Linear(input_size, hidden_size)
        self.Cl = nn.Linear(input_size, hidden_size)

        self.output_last = output_last

    def step(self, input, Hidden_State, Cell_State):
        conv_sample_start = time.time()
        conv = self.gconv(input)
        conv_sample_end = time.time()
        # print('conv_sample:', (conv_sample_end - conv_sample_start))
        combined = torch.cat((conv, Hidden_State), 1)
        f = torch.sigmoid(self.fl(combined))
        i = torch.sigmoid(self.il(combined))
        o = torch.sigmoid(self.ol(combined))
        C = torch.tanh(self.Cl(combined))
        Cell_State = f * Cell_State + i * C
        Hidden_State = o * torch.tanh(Cell_State)

        return Hidden_State, Cell_State

    def Bi_torch(self, a):
        a[a < 0] = 0
        a[a > 0] = 1
        return a

    def forward(self, inputs):
        batch_size = inputs.size(0)
        time_step = inputs.size(1)
        Hidden_State, Cell_State = self.initHidden(batch_size)

        outputs = None

        train_sample_start = time.time()

        for i in range(time_step):
            Hidden_State, Cell_State = self.step(torch.squeeze(inputs[:, i:i + 1, :]), Hidden_State, Cell_State)

            if outputs is None:
                outputs = Hidden_State.unsqueeze(1)
            else:
                outputs = torch.cat((outputs, Hidden_State.unsqueeze(1)), 1)

        train_sample_end = time.time()
        # print('train sample:' , (train_sample_end - train_sample_start))
        if self.output_last:
            return outputs[:, -1, :]
        else:
            return outputs

    def initHidden(self, batch_size):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size))
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size))
            return Hidden_State, Cell_State

    def reinitHidden(self, batch_size, Hidden_State_data, Cell_State_data):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(Hidden_State_data.cuda(), requires_grad=True)
            Cell_State = Variable(Cell_State_data.cuda(), requires_grad=True)
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(Hidden_State_data, requires_grad=True)
            Cell_State = Variable(Cell_State_data, requires_grad=True)
            return Hidden_State, Cell_State


class GraphConvolutionalLSTM(nn.Module):

    def __init__(self, K, A, FFR, feature_size, Clamp_A=True, output_last=True):
        '''
        Args:
            K: K-hop graph
            A: adjacency matrix
            FFR: free-flow reachability matrix
            feature_size: the dimension of features
            Clamp_A: Boolean value, clamping all elements of A between 0. to 1.
        '''
        super(GraphConvolutionalLSTM, self).__init__()
        self.feature_size = feature_size
        self.hidden_size = feature_size

        self.K = K

        self.A_list = []  # Adjacency Matrix List
        A = torch.FloatTensor(A)
        A_temp = torch.eye(feature_size, feature_size)
        for i in range(K):
            A_temp = torch.matmul(A_temp, torch.Tensor(A))
            if Clamp_A:
                # confine elements of A
                A_temp = torch.clamp(A_temp, max=1.)
            self.A_list.append(torch.mul(A_temp, torch.Tensor(FFR)))
        #             self.A_list.append(A_temp)

        # a length adjustable Module List for hosting all graph convolutions
        self.gc_list = nn.ModuleList(
            [FilterLinear(feature_size, feature_size, self.A_list[i], bias=False) for i in range(K)])

        hidden_size = self.feature_size
        input_size = self.feature_size * K

        self.fl = nn.Linear(input_size + hidden_size, hidden_size)
        self.il = nn.Linear(input_size + hidden_size, hidden_size)
        self.ol = nn.Linear(input_size + hidden_size, hidden_size)
        self.Cl = nn.Linear(input_size + hidden_size, hidden_size)

        # initialize the neighbor weight for the cell state
        self.Neighbor_weight = Parameter(torch.FloatTensor(feature_size))
        stdv = 1. / math.sqrt(feature_size)
        self.Neighbor_weight.data.uniform_(-stdv, stdv)

        self.output_last = output_last

    def step(self, input, Hidden_State, Cell_State):

        x = input

        gc = self.gc_list[0](x)
        for i in range(1, self.K):
            gc = torch.cat((gc, self.gc_list[i](x)), 1)

        combined = torch.cat((gc, Hidden_State), 1)
        f = torch.sigmoid(self.fl(combined))
        i = torch.sigmoid(self.il(combined))
        o = torch.sigmoid(self.ol(combined))
        C = torch.tanh(self.Cl(combined))

        NC = torch.mul(Cell_State,
                       torch.mv(Variable(self.A_list[-1], requires_grad=False).cuda(), self.Neighbor_weight))
        Cell_State = f * NC + i * C
        Hidden_State = o * torch.tanh(Cell_State)

        return Hidden_State, Cell_State, gc

    def Bi_torch(self, a):
        a[a < 0] = 0
        a[a > 0] = 1
        return a

    def forward(self, inputs):
        batch_size = inputs.size(0)
        time_step = inputs.size(1)
        Hidden_State, Cell_State = self.initHidden(batch_size)

        outputs = None

        for i in range(time_step):
            Hidden_State, Cell_State, gc = self.step(torch.squeeze(inputs[:, i:i + 1, :]), Hidden_State, Cell_State)

            if outputs is None:
                outputs = Hidden_State.unsqueeze(1)
            else:
                outputs = torch.cat((outputs, Hidden_State.unsqueeze(1)), 1)

        if self.output_last:
            return outputs[:, -1, :]
        else:
            return outputs

    def initHidden(self, batch_size):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size).cuda())
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(torch.zeros(batch_size, self.hidden_size))
            Cell_State = Variable(torch.zeros(batch_size, self.hidden_size))
            return Hidden_State, Cell_State

    def reinitHidden(self, batch_size, Hidden_State_data, Cell_State_data):
        use_gpu = torch.cuda.is_available()
        if use_gpu:
            Hidden_State = Variable(Hidden_State_data.cuda(), requires_grad=True)
            Cell_State = Variable(Cell_State_data.cuda(), requires_grad=True)
            return Hidden_State, Cell_State
        else:
            Hidden_State = Variable(Hidden_State_data, requires_grad=True)
            Cell_State = Variable(Cell_State_data, requires_grad=True)
            return Hidden_State, Cell_State


## New Models

def generate_post_mp(final_pred_len, final_pred_hidden, feature_size):
    final_pred = []
    if final_pred_len > 1:
        final_pred.append(nn.Linear(feature_size, final_pred_hidden))
        final_pred.append(nn.ReLU())
        for i in range(final_pred_len - 2):
            final_pred.append(nn.Linear(final_pred_hidden, final_pred_hidden))
            final_pred.append(nn.ReLU())
        final_pred.append(nn.Linear(final_pred_hidden, 1))
        final_pred.append(nn.Sigmoid())
    else:
        final_pred.append(nn.Linear(feature_size, 1))
        final_pred.append(nn.Sigmoid())
    return nn.Sequential(*final_pred)

class LSTM_GAT(nn.Module):
    def __init__(self, K, num_nodes, batch, feature_size, lstm_hidden_size, seq_len=10, heads=1, dropout=0, final_pred_len=1, final_pred_hidden=0):
        # K is num layers (or hops on graph) of convolution
        # TODO: dataloader changes to allow for multiple As

        super(LSTM_GAT, self).__init__()
        self.seq_len = seq_len
        self.num_nodes = num_nodes
        self.K = K
        self.batch = batch


        self.convs = nn.ModuleList()
        self.convs.append(GATConv(lstm_hidden_size, feature_size, heads=heads))
        for i in range(K - 1):
            self.convs.append(GATConv(feature_size * heads, feature_size, heads=heads))
        self.pre_mp = nn.LSTM(1, lstm_hidden_size)
        self.post_mp = generate_post_mp(final_pred_len, final_pred_hidden, feature_size * heads)
        self.dropout = dropout

    def forward(self, x, edge_index):
        x: torch.Tensor

        #print(x.shape) # BATCH x SEQ x NUM_NODES
        x = x.permute(1, 0, 2) # SEQ x BATCH x NUM_NODES
        x = x.view(self.seq_len, -1, 1) # SEQ x (BATCH x NUM_NODES) x 1
        x = self.pre_mp(x)[1][0][0] # output of (40 x 323) x lstm_size, last layer output

        for i in range(self.K):
            x = self.convs[i](x, edge_index)
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout)

        x = self.post_mp(x)
        x = x.T
        return x
        # lstm will have matching output sequence len, we just want last bit.

class GATLSTM(nn.Module):
    def __init__(self, K, As, feature_size, lstm_hidden_size, seq_len=10, heads=1, dropout=0):
        # K is num layers (or hops on graph) of convolution
        # TODO: dataloader changes to allow for multiple As

        super(GATLSTM, self).__init__()
        self.seq_len = seq_len
        self.num_nodes = len(As[0])
        self.feature_size = feature_size
        self.lstm_hidden_size = lstm_hidden_size
        self.K = K

        self.convs = nn.ModuleList()
        self.convs.append(GATConv(1, feature_size, heads=heads))
        for i in range(K - 1):
            self.convs.append(GATConv(feature_size, feature_size, heads=heads))
        self.final_pred = nn.ModuleList([nn.LSTM(feature_size, lstm_hidden_size), nn.Linear(lstm_hidden_size, 1)])
        self.dropout = dropout

    def forward(self, data):
        x: torch.Tensor
        x, edge_index, batch = data.x, data.edge_index, data.batch
        # print(x.shape) # should be seq_size by num nodes

        cur_device = data.x.device

        all_gat_outputs = torch.empty((self.seq_len, self.num_nodes, self.feature_size)).to(cur_device)
        for i in range(self.seq_len):
            gat_input = x[i].view(-1, 1) # make it 323 x 1 instead of 1 x 323 so gat knows 323 is node count
            gat_output = gat_input
            for j in range(self.K):
                gat_output = self.convs[j](gat_output, edge_index)
                gat_output = F.relu(gat_output)
                gat_output = F.dropout(gat_output, p=self.dropout)
            all_gat_outputs[i] = gat_output


        # x = x.view(self.seq_len, -1, self.num_nodes)
        x = self.final_pred[0](all_gat_outputs) # x[0] is output, x[0][-1] is final output of shape num_nodes x hidden size
        final_lstm_output = x[0][-1]
        return self.final_pred[1](final_lstm_output).T
        # lstm will have matching output sequence len, we just want last bit.

class GCNLSTM(nn.Module):
    def __init__(self, K, num_nodes, feature_size, lstm_hidden_size, seq_len=10, dropout=0):
        # K is num layers (or hops on graph) of convolution
        # TODO: dataloader changes to allow for multiple As

        super(GCNLSTM, self).__init__()
        self.seq_len = seq_len
        self.num_nodes = num_nodes
        self.feature_size = feature_size
        self.lstm_hidden_size = lstm_hidden_size
        self.K = K

        self.convs = nn.ModuleList()
        self.convs.append(GCNConv(1, feature_size))
        for i in range(K - 1):
            self.convs.append(GCNConv(feature_size, feature_size))
        self.final_pred = nn.ModuleList([nn.LSTM(feature_size, lstm_hidden_size), nn.Linear(lstm_hidden_size, 1)])
        self.dropout = dropout

    def forward(self, data):
        x: torch.Tensor
        x, edge_index, batch = data.x, data.edge_index, data.batch
        # print(x.shape) # should be seq_size by num nodes

        cur_device = data.x.device

        x = x.view(-1, self.seq_len, self.num_nodes)
        all_gcn_outputs = torch.empty((40, self.seq_len, self.num_nodes, self.feature_size)).to(cur_device)
        for i in range(self.seq_len):
            gcn_input = x[:, i].view(40, 323) # 40 x 323
            # gcn_input = gcn_input.T
            gcn_input = gcn_input.view(-1, 1) # make it 323 x 1 instead of 1 x 323 so gat knows 323 is node count
            gcn_output = gcn_input
            for j in range(self.K):
                gcn_output = self.convs[j](gcn_output, edge_index)
                gcn_output = F.relu(gcn_output)
                gcn_output = F.dropout(gcn_output, p=self.dropout)
            all_gcn_outputs[i] = gcn_output


        # x = x.view(self.seq_len, -1, self.num_nodes)
        x = self.final_pred[0](all_gcn_outputs) # x[0] is output, x[0][-1] is final output of shape num_nodes x hidden size
        final_lstm_output = x[0][-1]
        return self.final_pred[1](final_lstm_output).T
        # lstm will have matching output sequence len, we just want last bit.

class LSTM_in_GCN_conv(MessagePassing):
    def __init__(self, in_channels, out_channels, aggr, prop_cx, **kwargs):
        super(LSTM_in_GCN_conv, self).__init__(aggr=aggr, **kwargs)

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.cell = LSTMCell(in_channels, out_channels)
        self.prop_cx = prop_cx

    def forward(self, x, edge_index, hx, cx, edge_weight=None):
        """"""

        if hx is None or cx is None:
            hx, cx = self.cell.forward(x)
        else:
            hx, cx = self.cell.forward(x, (hx, cx))



        if not self.prop_cx:
            return self.propagate(edge_index, x=hx), cx
        else:
            return self.propagate(edge_index, x=torch.cat((hx, cx), dim=-1))


    def message(self, x_j):
        return x_j

    def update(self, aggr_out):
        if not self.prop_cx:
            return aggr_out
        else:
            return torch.split(aggr_out, self.out_channels, dim=-1)

class Custom_LSTM(nn.Module):
    def __init__(self, num_nodes, feature_size, seq_len=10, aggr='add', prop_cx=False, final_pred_len=1, final_pred_hidden=1):
        # TODO: dataloader changes to allow for multiple As

        super(Custom_LSTM, self).__init__()
        self.seq_len = seq_len
        self.num_nodes = num_nodes
        self.feature_size = feature_size

        self.conv = LSTM_in_GCN_conv(1, feature_size, aggr=aggr, prop_cx=prop_cx)
        self.final_pred = generate_post_mp(final_pred_len, final_pred_hidden, feature_size)

    def forward(self, x, edge_index):
        x: torch.Tensor
        x, edge_index = x, edge_index
        # print(x.shape) # should be seq_size by num nodes
        hx = None
        cx = None
        x = x.permute(1, 0, 2) # seq, batch, num_nodes
        num_nodes = x.shape[2]
        batch_size = x.shape[1]

        for i in range(self.seq_len):
            b_i_input = x[i].view(batch_size, num_nodes, 1)
            hx, cx = self.conv(b_i_input, edge_index, hx, cx)

        final_preds = self.final_pred(hx)

        return final_preds.view(-1, self.num_nodes)
        # lstm will have matching output sequence len, we just want last bit.

class LSTMGCN(nn.Module):
    def __init__(self, K, A, feature_size, lstm_hidden_size, seq_len=10, heads=1, dropout=0.0, one_conv=False):
        # K is num layers (or hops on graph) of convolution
        # TODO: dataloader changes to allow for multiple As

        super(LSTMGCN, self).__init__()
        self.seq_len = seq_len
        self.num_nodes = len(A)
        self.feature_size = feature_size
        self.lstm_hidden_size = lstm_hidden_size
        self.dropout = dropout
        self.one_conv = one_conv
        self.K = K


        self.lstm = nn.LSTM(1, lstm_hidden_size)
        self.final_prediction = nn.Sequential(nn.Linear(feature_size * heads, 1), nn.Sigmoid())



        if one_conv:
            self.convs = GCNConv(lstm_hidden_size, feature_size)
        else:
            self.convs = nn.ModuleList()
            self.convs.append(GCNConv(lstm_hidden_size, feature_size))
            for _ in range(K-1):
                self.convs.append(GCNConv(feature_size * heads, feature_size))


    def forward(self, data):
        x: torch.Tensor
        x, edge_index, batch = data.x, data.edge_index, data.batch
        # print(x.shape) # should be seq_size x batch_size by num nodes

        x = x.view(self.seq_len, -1, self.num_nodes) # 10 x 40 x 323
        x = x.view(self.seq_len, -1, 1) # 10 x (40 x 323)
        x = self.lstm(x)[1][0][0] # output of (40 x 323) x lstm_size, last layer output
        # x = x.view(-1, self.num_nodes, self.lstm_hidden_size)

        if self.one_conv:
            x = self.convs(x, edge_index)
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout)
        else:
            for i in range(self.K):
                x = self.convs[i](x, edge_index)
                x = F.relu(x)
                x = F.dropout(x, p=self.dropout)

        x = self.final_prediction(x)
        return x.view(-1, self.num_nodes)

class LSTMGIN(nn.Module):
    def __init__(self, K, A, feature_size, seq_len=10, dropout=0.0, one_conv=False):
        # K is num layers (or hops on graph) of convolution
        # TODO: dataloader changes to allow for multiple As

        super(LSTMGIN, self).__init__()
        self.seq_len = seq_len
        self.num_nodes = len(A)
        self.feature_size = feature_size
        self.dropout = dropout
        self.one_conv = one_conv


        self.lstm = nn.LSTM(1, feature_size)
        self.final_prediction = nn.Sequential(nn.Linear(feature_size, 1), nn.Sigmoid())

        self.thetaF = nn.Sequential(
            nn.Linear(feature_size, feature_size),
            nn.ReLU(),
            nn.Linear(feature_size, feature_size),
            nn.ReLU(),
            nn.Linear(feature_size, feature_size),
            nn.ReLU(),
            nn.Linear(feature_size, feature_size),
            nn.ReLU(),
        )

        if one_conv:
            self.convs = GINConv(feature_size, feature_size)
        else:
            self.convs = nn.ModuleList()
            self.convs.append(GINConv(self.thetaF))
            for _ in range(K-1):
                self.convs.append(GINConv(self.thetaF))


    def forward(self, data):
        x: torch.Tensor
        x, edge_index, batch = data.x, data.edge_index, data.batch
        # print(x.shape) # should be seq_size x batch_size by num nodes

        x = x.view(self.seq_len, -1, self.num_nodes) # 10 x 40 x 323
        x = x.view(self.seq_len, -1, 1) # 10 x (40 x 323)
        x = self.lstm(x)[1][0][0] # output of (40 x 323) x lstm_size, last layer output
        # x = x.view(-1, self.num_nodes, self.lstm_hidden_size)

        if self.one_conv:
            x = self.convs(x, edge_index)
            x = F.relu(x)
            x = F.dropout(x, p=self.dropout)
        else:
            for i in range(self.K):
                x = self.convs[i](x, edge_index)
                x = F.relu(x)
                x = F.dropout(x, p=self.dropout)

        x = self.final_prediction(x)
        return x.view(-1, self.num_nodes)