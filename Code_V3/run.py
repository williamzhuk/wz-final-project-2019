from random import sample

from Code.dataset_prep import PrepareDataset, PrepareGraphDataset, GenAltFFR, LoadDataset
from Code.models import *
from Code.model_training import *
from matplotlib import pyplot as plt
from os import listdir

dataset_dict = LoadDataset()
speed_matrix = dataset_dict['speed_matrix']
A = dataset_dict['A']
FFR = dataset_dict['FFR']
loop_nodes = dataset_dict['loop_nodes']
FFR_alts = GenAltFFR(loop_nodes, FFR[0])

# lstm = LSTM(input_dim, hidden_dim, output_dim, output_last = True)
# lstm, lstm_loss = TrainModel(lstm, train_dataloader, valid_dataloader, num_epochs = 10)
# lstm_test = TestModel(lstm, test_dataloader, max_speed )

# %%

# K = 64
# Clamp_A = False
# lsgclstm = LocalizedSpectralGraphConvolutionalLSTM(K, torch.Tensor(A), A.shape[0], Clamp_A=Clamp_A, output_last = True)
# lsgclstm, lsgclstm_loss = TrainModel(lsgclstm, train_dataloader, valid_dataloader, num_epochs = 10)
# lsgclstm_test = TestModel(lsgclstm, test_dataloader, max_speed )


num_nodes = len(A)

edge_matrix_dict = {
    'A': A,
    'Default FFR': FFR[0],
    'FFR-20': FFR_alts[20][0],
    'FFR-40': FFR_alts[40][0],
    'FFR-60': FFR_alts[60][0],
    'FFR-80': FFR_alts[80][0],
}

hypervars = {
    'feature_size': [2, 4, 8, 16, 32, 64, 128],
    'seq_len': [5, 10, 15, 20, 25, 30, 35, 40],
    'aggr': ['add', 'mean', 'max'],
    'prop_cx': [True, False],
    'final_pred_len': range(1, 9, 1),
    'final_pred_hidden': [2, 4, 8, 16, 32, 64, 128],
    'Edge_Matrix': ['A', 'Default FFR', 'FFR-20', 'FFR-40', 'FFR-60', 'FFR-80'],
    'Batch': [1],
    'hops_prepare': range(1, 17, 1),
    'learning_rate': list(np.power(10, np.arange(-4.0, -5.0, -.01))),
    'patience': [2],
    'optimizer': [torch.optim.Adam, torch.optim.RMSprop],
    'heads': [1, 2, 4, 8],
    'num_convs': range(1, 10, 1),
    'dropout': list(np.arange(0.0, 0.5, .01))
}

def my_hypervars():
    hyper_sample = {
        'feature_size': 32,
        'seq_len': 10,
        'aggr': 'add',
        'prop_cx': False,
        'final_pred_len': 2,
        'final_pred_hidden': 32,
        'Edge_Matrix': 'A',
        'Batch': 10,
        'learning_rate': 1e-5,
        'patience': 10,
        'optimizer': torch.optim.RMSprop
    }
    return hyper_sample

def sample_hypervars():
    hyper_sample = {}
    for key, val in hypervars.items():
        hyper_sample[key] = sample(val, 1)[0]
    return hyper_sample

def test_custom_model_with_hypervars(hypervar_dict):

    print("Picked sample: ", hypervar_dict)
    edge_matrix = edge_matrix_dict[hypervar_dict['Edge_Matrix']]
    using_FFR = not np.array_equal(A, edge_matrix)

    edge_index, train_dataloader, valid_dataloader, test_dataloader = PrepareGraphDataset(speed_matrix,
                                                                                          A,
                                                                                          hypervar_dict['hops_prepare'],
                                                                                          edge_matrix,
                                                                                          using_FFR,
                                                                                          seq_len=hypervar_dict[
                                                                                              'seq_len'],
                                                                                          BATCH_SIZE=hypervar_dict[
                                                                                              'Batch'])

    model = Custom_LSTM(num_nodes,
                        hypervar_dict['feature_size'],
                        hypervar_dict['seq_len'],
                        hypervar_dict['aggr'],
                        hypervar_dict['prop_cx'],
                        hypervar_dict['final_pred_len'],
                        hypervar_dict['final_pred_hidden'])

    model, loss = TrainGraphModel(model,
                                  train_dataloader,
                                  valid_dataloader,
                                  edge_index,
                                  hypervar_dict['optimizer'],
                                  num_epochs=10,
                                  learning_rate=hypervar_dict['learning_rate'],
                                  patience=hypervar_dict['patience'])

    losses = loss[3]
    f = open("hypervars_test.txt", "a+")
    f.write(str(hypervar_dict) + '\r\n')
    f.writelines([str(loss.item()) + '\r\n' for loss in losses])
    f.write('\r\n \r\n')
    f.close()

def test_GAN_hypervars(hypervar_dict):
    print("Picked sample: ", hypervar_dict)

    edge_matrix = edge_matrix_dict[hypervar_dict['Edge_Matrix']]
    using_FFR = not np.array_equal(A, edge_matrix)

    model = LSTM_GAT(hypervar_dict['num_convs'],
                     num_nodes,
                     hypervar_dict['Batch'],
                     hypervar_dict['feature_size'],
                     hypervar_dict['feature_size'], #going to have same for lstm as graph
                     hypervar_dict['seq_len'],
                     hypervar_dict['heads'],
                     hypervar_dict['dropout'],
                     hypervar_dict['final_pred_len'],
                     hypervar_dict['final_pred_hidden'],
                     )

    edge_index, train_dataloader, valid_dataloader, test_dataloader = PrepareGraphDataset(speed_matrix,
                                                                                          A,
                                                                                          hypervar_dict['hops_prepare'],
                                                                                          edge_matrix,
                                                                                          using_FFR,
                                                                                          seq_len=hypervar_dict['seq_len'],
                                                                                          BATCH_SIZE=hypervar_dict['Batch'])

    model, loss = TrainGraphModel(model,
                                  train_dataloader,
                                  valid_dataloader,
                                  edge_index,
                                  torch.optim.Adam,
                                  num_epochs=10,
                                  learning_rate=hypervar_dict['learning_rate'],
                                  patience=8)

    losses = loss[3]
    f = open("hypervars_GAT_test.txt", "a+")
    f.write("LSTM GAT" + '\r\n')
    f.writelines([str(loss.item()) + '\r\n' for loss in losses])
    f.write('\r\n \r\n')
    f.close()

def run_my_hpervars():
    sam = my_hypervars()
    test_custom_model_with_hypervars(sam)

def run_manual_model():


    K = 4
    edge_matrix = edge_matrix_dict['Default FFR']
    using_FFR = True
    seq_len = 10
    batch_size = 1
    feature_size = 32
    lstm_hidden_size = 32
    heads = 8
    dropout = 0


    model = LSTM_GAT(K, num_nodes, batch_size, feature_size, lstm_hidden_size, seq_len, heads, dropout)

    edge_index, train_dataloader, valid_dataloader, test_dataloader = PrepareGraphDataset(speed_matrix,
                                                                                          A,
                                                                                          K,
                                                                                          edge_matrix,
                                                                                          using_FFR,
                                                                                          seq_len=seq_len,
                                                                                          BATCH_SIZE=batch_size)

    model, loss = TrainGraphModel(model,
                                  train_dataloader,
                                  valid_dataloader,
                                  edge_index,
                                  torch.optim.Adam,
                                  num_epochs=10,
                                  learning_rate=2e-5,
                                  patience=8)

    losses = loss[3]
    f = open("manual_model.txt", "a+")
    f.write("LSTM GAT" + '\r\n')
    f.writelines([str(loss.item()) + '\r\n' for loss in losses])
    f.write('\r\n \r\n')
    f.close()


def test_hypervars():
    while (True):
        sam = sample_hypervars()
        # try:
        # test_custom_model_with_hypervars(sam)
        test_GAN_hypervars(sam)
        # except Exception as e:
        #     print(e)

# run_my_hpervars()
test_hypervars()
# run_manual_model()

def read_hypervars_test():
    f = open("hypervars_test.txt", "r")

    hypervar_str = None
    results = {}

    for line in f.readlines():
        if "{" in line:
            hypervar_str = line
            results[hypervar_str] = []
        elif len(line) > 4:
            results[hypervar_str].append(float(line))

    return results

# results_dict = read_hypervars_test()
#
# files = listdir('./')
# hgclstm_val_losses = {}
# gclstm_val_loss_loaded = False
# for file in files:
#     if 'val_loss_s' in file:
#         hgclstm_val_losses[file[9:-4]] = np.load(file, allow_pickle=True)
#
# if 'hgclstm_val_loss.npy' in files:
#     hgclstm_val_loss = np.load('hgclstm_val_loss.npy', allow_pickle=True)
#     gclstm_val_loss_loaded = True
#
# if not gclstm_val_loss_loaded:
#     K = 3
#     back_length = 3
#     Clamp_A = False
#     train_dataloader, valid_dataloader, test_dataloader, max_speed = PrepareDataset(speed_matrix)
#     gclstm = GraphConvolutionalLSTM(K, torch.Tensor(A), FFR[back_length], A.shape[0], Clamp_A=Clamp_A, output_last = True)
#     gclstm, gclstm_loss = TrainModel(gclstm, train_dataloader, valid_dataloader, num_epochs = 80)
#     gclstm_test = TestModel(gclstm, test_dataloader, max_speed)
#     hgclstm_val_loss = np.asarray(gclstm_loss[3])
#     np.save('hgclstm_val_loss', hgclstm_val_loss)
#
# fig, ax = plt.subplots()
# plt.plot(np.arange(1, len(hgclstm_val_loss) + 1),hgclstm_val_loss,  label = 'Default (60-3-3)')
# for name, loss in results_dict.items():
#     plt.plot(np.arange(1, len(loss) + 1), loss, label=name)
# for name, loss in hgclstm_val_losses.items():
#     plt.plot(np.arange(1, len(loss) + 1),loss,  label = name)
# # plt.ylim((6 * 0.0001, 0.0019))
# plt.xticks(fontsize=12)
# plt.yticks(fontsize=12)
# plt.yscale('log')
# plt.ylabel('Validation Loss (MSE)', fontsize=12)
# plt.xlabel('Epoch', fontsize=12)
# # plt.gca().invert_xaxis()
# # plt.legend(fontsize=8)
# plt.grid(True, which='both')
#
# plt.savefig('Validation_loss.png', dpi=300, bbox_inches='tight', pad_inches=0.1)
#
# # %%
