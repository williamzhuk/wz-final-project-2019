from random import shuffle

import torch.utils.data as utils
import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from torch_geometric.data import Data, DataLoader


def LoadDataset():
    #     data = 'inrix'
    data = 'loop'
    directory = '../../Data_Warehouse/Data_network_traffic/'

    speed_matrix = pd.read_pickle(directory + 'speed_matrix_2015')
    A = np.load(directory + 'Loop_Seattle_2015_A.npy')
    FFR_5min = np.load(directory + 'Loop_Seattle_2015_reachability_free_flow_5min.npy')
    FFR_10min = np.load(directory + 'Loop_Seattle_2015_reachability_free_flow_10min.npy')
    FFR_15min = np.load(directory + 'Loop_Seattle_2015_reachability_free_flow_15min.npy')
    FFR_20min = np.load(directory + 'Loop_Seattle_2015_reachability_free_flow_20min.npy')
    FFR_25min = np.load(directory + 'Loop_Seattle_2015_reachability_free_flow_25min.npy')
    FFR = [FFR_5min, FFR_10min, FFR_15min, FFR_20min, FFR_25min]
    loop_nodes = pd.read_csv(directory + 'nodes_loop_mp_list.csv')  # WZ

    return {
        'speed_matrix': speed_matrix,
        'A': A,
        'FFR': FFR,
        'loop_nodes': loop_nodes
    }


# def plot_speeds(speed_matrix, loop_nodes):
#     # Plots the speeds of first 288 5 minute averages for all nodes
#     for label, content in speed_matrix.items():
#         indices = []
#         values = []
#
#         for index, value in content.iteritems():
#             time = dt.strptime(index, '%Y-%m-%d %H:%M:%S')
#             indices.append(time.day * 24 + time.hour + time.minute / 60.0)
#             values.append(value)
#             if len(indices) > 288 * 2:
#                 break
#
#         plt.plot(indices, values, label=label)
#     plt.ylabel("speed (mph)")
#     plt.xlabel("time (hours)")
#     plt.savefig('Speeds.png', dpi=300, bbox_inches='tight', pad_inches=0.1)
#
#
#     print(loop_nodes)
#     print(loop_nodes.get_value(1, 'milepost'))
#     assert isinstance(speed_matrix, pd.DataFrame)
#     print(speed_matrix)
#     print(speed_matrix.max(axis=0).mean())





def name_to_highway(name: str):
    return name[:6]


def name_to_milepost(name: str):
    return int(name[6:]) / 100.0


def distance_allowed(speed: float, time_in_min: float):
    return speed / 60 * time_in_min

def is_within_distance(milepost1, milepost2, distance):
    return abs(milepost1 - milepost2) < distance


def diff_roads_within_distance(mp1, mp2, c1, c2, distance):
    return abs(mp1 - c1) + abs(mp2 - c2) < distance


FFR_alts = {}


def estimate_connections(loop_nodes, FFR_5min):
    FFR_no_inter_connections = np.zeros_like(FFR_5min)
    inter_road_connections = {}
    estimated_connection_points = {}
    for i1, v1 in loop_nodes["milepost"].items():
        for i2, v2 in loop_nodes["milepost"].items():
            hw1 = name_to_highway(v1)
            hw2 = name_to_highway(v2)
            mp1 = name_to_milepost(v1)
            mp2 = name_to_milepost(v2)
            is_same_highway = hw1 == hw2  # can use 1: to allow rubbernecking.
            if is_same_highway and is_within_distance(mp1, mp2, distance_allowed(60, 5)):
                FFR_no_inter_connections[i1][i2] = 1
            if not is_same_highway and FFR_no_inter_connections[i1][i2] < FFR_5min[i1][i2]:
                # Connection we do not see
                if hw1 not in inter_road_connections:
                    inter_road_connections[hw1] = {}
                if hw2 not in inter_road_connections[hw1]:
                    inter_road_connections[hw1][hw2] = []
                inter_road_connections[hw1][hw2] += [(mp1, mp2)]

    for hw1, hw2_dict in inter_road_connections.items():
        estimated_connection_points[hw1] = {}
        for hw2, mp_list in hw2_dict.items():
            mp1_estimate = 0.0
            mp2_estimate = 0.0
            for mp1, mp2 in mp_list:
                mp1_estimate += mp1
                mp2_estimate += mp2
            mp1_estimate /= len(mp_list)
            mp2_estimate /= len(mp_list)
            estimated_connection_points[hw1][hw2] = (mp1_estimate, mp2_estimate)
    return estimated_connection_points



def GenAltFFR(loop_nodes, FFR_5min):
    estimated_connection_points = estimate_connections(loop_nodes, FFR_5min)
    FFR_alts = {}
    for prop_speed in [20, 40, 60, 80]:
        # Generate new FFR for propagation speed

        new_FFR_5min = np.zeros_like(FFR_5min)
        for i1, v1 in loop_nodes["milepost"].items():
            for i2, v2 in loop_nodes["milepost"].items():
                hw1 = name_to_highway(v1)
                hw2 = name_to_highway(v2)
                mp1 = name_to_milepost(v1)
                mp2 = name_to_milepost(v2)
                distance = distance_allowed(prop_speed, 5)
                is_same_highway = hw1 == hw2  # can use 1: to allow rubbernecking.
                if is_same_highway and is_within_distance(mp1, mp2, distance):
                    new_FFR_5min[i1][i2] = 1
                elif hw1 in estimated_connection_points and hw2 in estimated_connection_points[hw1]:
                    c1, c2 = estimated_connection_points[hw1][hw2]
                    if diff_roads_within_distance(mp1, mp2, c1, c2, distance):
                        new_FFR_5min[i1][i2] = 1

        old_num_edges = np.sum(FFR_5min)
        new_num_edges = np.sum(new_FFR_5min)
        edge_diff = np.sum(np.abs(new_FFR_5min - FFR_5min))
        total_unaccounted_for = edge_diff - abs((old_num_edges - new_num_edges))

        print(old_num_edges, new_num_edges, edge_diff, total_unaccounted_for)

        pseudo_10 = np.linalg.matrix_power(new_FFR_5min, 2) + (FFR_5min if prop_speed * 2 >= 60 else 0)
        pseudo_15 = np.matmul(pseudo_10, new_FFR_5min) + (FFR_5min if prop_speed * 3 >= 60 else 0)
        pseudo_20 = np.matmul(pseudo_15, new_FFR_5min) + (FFR_5min if prop_speed * 4 >= 60 else 0)
        pseudo_25 = np.matmul(pseudo_20, new_FFR_5min) + (FFR_5min if prop_speed * 5 >= 60 else 0)

        for i1, v1 in loop_nodes["milepost"].items():
            for i2, v2 in loop_nodes["milepost"].items():
                if pseudo_10[i1][i2] > 0:
                    pseudo_10[i1][i2] = 1
                if pseudo_15[i1][i2] > 0:
                    pseudo_15[i1][i2] = 1
                if pseudo_20[i1][i2] > 0:
                    pseudo_20[i1][i2] = 1
                if pseudo_25[i1][i2] > 0:
                    pseudo_25[i1][i2] = 1

        FFR_alts[prop_speed] = [new_FFR_5min, pseudo_10, pseudo_15, pseudo_20, pseudo_25]
    return FFR_alts
# %%


def make_A_k(A_0, k, FFR_matrix, Clamp_A = True):
    A_list = []  # Adjacency Matrix List
    A_0 = torch.FloatTensor(A_0)
    A_temp = torch.eye(len(A_0), len(A_0))
    for i in range(k):
        A_temp = torch.matmul(A_temp, torch.Tensor(A_0))
        if Clamp_A:
            # confine elements of A
            A_temp = torch.clamp(A_temp, max=1.)
        A_list.append(torch.mul(A_temp, torch.Tensor(FFR_matrix)))
    return A_list[-1]


def make_edge_index(A):  # A can be modified with FFR or other
    num_nodes = len(A)
    edge_index = [[], []]
    for src in range(num_nodes):
        for dst in range(num_nodes):
            if A[src][dst] > 0:
                edge_index[0].append(src)
                edge_index[1].append(dst)
    return torch.from_numpy(np.array(edge_index))


def to_float(tensor):
    return tensor.type(torch.FloatTensor)

def PrepareDataset(speed_matrix, seq_len=10, BATCH_SIZE=40, pred_len=1, train_propotion=0.7, valid_propotion=0.2):
    """ Prepare training and testing datasets and dataloaders.

    Convert speed/volume/occupancy matrix to training and testing dataset.
    The vertical axis of speed_matrix is the time axis and the horizontal axis
    is the spatial axis.

    Args:
        speed_matrix: a Matrix containing spatial-temporal speed data for a network
        seq_len: length of input sequence
        pred_len: length of predicted sequence
    Returns:
        Training dataloader
        Testing dataloader
    """


    time_len = speed_matrix.shape[0]

    max_speed = speed_matrix.max().max()
    speed_matrix = speed_matrix / max_speed

    speed_sequences, speed_labels = [], []
    for i in range(time_len - seq_len - pred_len):
        speed_sequences.append(speed_matrix.iloc[i:i + seq_len].values)
        speed_labels.append(speed_matrix.iloc[i + seq_len:i + seq_len + pred_len].values)
    speed_sequences, speed_labels = np.asarray(speed_sequences), np.asarray(speed_labels)

    # shuffle and split the dataset to training and testing datasets
    sample_size = speed_sequences.shape[0]
    # index = np.arange(sample_size, dtype=int)
    # np.random.shuffle(index)

    train_index = int(np.floor(sample_size * train_propotion))
    valid_index = int(np.floor(sample_size * (train_propotion + valid_propotion)))

    train_data, train_label = speed_sequences[:train_index], speed_labels[:train_index]
    valid_data, valid_label = speed_sequences[train_index:valid_index], speed_labels[train_index:valid_index]
    test_data, test_label = speed_sequences[valid_index:], speed_labels[valid_index:]


    train_data, train_label = to_float(torch.from_numpy(train_data)), to_float(torch.from_numpy(train_label))
    valid_data, valid_label = to_float(torch.from_numpy(valid_data)), to_float(torch.from_numpy(valid_label))
    test_data, test_label = to_float(torch.from_numpy(test_data)), to_float(torch.from_numpy(test_label))

    train_dataset = utils.TensorDataset(train_data, train_label)
    valid_dataset = utils.TensorDataset(valid_data, valid_label)
    test_dataset = utils.TensorDataset(test_data, test_label)

    train_dataloader = utils.DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
    valid_dataloader = utils.DataLoader(valid_dataset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)
    test_dataloader = utils.DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=True, drop_last=True)

    return train_dataloader, valid_dataloader, test_dataloader, max_speed

def PrepareGraphDataset(speed_matrix, A, K, FFR_5min, edges_using_FFR=False, seq_len=10, BATCH_SIZE=40, train_propotion=0.7, valid_propotion=0.2, cur_device=None):

    if cur_device is None:
        cur_device = cur_device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')


    max_speed = speed_matrix.max().max()
    speed_matrix = speed_matrix / max_speed

    num_nodes = speed_matrix.shape[1]

    edge_index = None
    if not edges_using_FFR:
        edge_index = make_edge_index(A)
    else:
        edge_index = make_edge_index(make_A_k(A, K, FFR_5min))

    torch_speeds = torch.tensor(speed_matrix.values)
    train_num = int(train_propotion * torch_speeds.shape[0])
    valid_num = int(valid_propotion * torch_speeds.shape[0])
    train = torch_speeds[:train_num]
    valid = torch_speeds[train_num:train_num + valid_num]
    test = torch_speeds[train_num + valid_num:]

    def dataset_generator(dataset, seq_len, batch_size):

        def dataset_looper():
            first_unusable_index = dataset.shape[0] - seq_len
            start_indices = list(range(0, first_unusable_index, 1))
            shuffle(start_indices)

            current = 0

            while current + batch_size < first_unusable_index:
                xs = torch.empty((batch_size, seq_len, num_nodes)).to(cur_device)
                ys = torch.empty((batch_size, num_nodes)).to(cur_device)

                indices = start_indices[current:current+batch_size]

                for index in indices:
                    xs[current%batch_size] = dataset[index:index+seq_len]
                    ys[current%batch_size] = dataset[index + seq_len]
                current += batch_size
                yield xs, ys

        return dataset_looper




    return edge_index, dataset_generator(train, seq_len, BATCH_SIZE), dataset_generator(valid, seq_len, BATCH_SIZE), dataset_generator(test, seq_len, BATCH_SIZE)